"""PyAudio Example: Play a WAVE file."""   """for console playing -python3 play.py output2.wav-"""

import pyaudio
import wave
import sys
import os.path
import time
from subprocess import call
import platform

CHUNK_SIZE=1024

def play_wav(wav_filename, chunk_size):
    '''
    Play (on the attached system sound device) the WAV file
    named wav_filename.
    '''

    if platform.system()=='Linux':
        #path = "/home/pi/pywork/"
        call(["omxplayer", "-o", "local",wav_filename])

        needed_list=["beep_beep.wav","bye.wav","empty_message.wav","hi.wav",
                     "please_reply.wav","beep.wav","no_message.wav","not_identified.wav","see_you.wav",
                     "not_valid_response.wav"]
        if wav_filename not in needed_list:
            call(["rm", wav_filename])
    else:
        try:
            print ('Trying to play file ' + wav_filename)
            wf = wave.open(wav_filename, 'rb')
        except IOError as ioe:
            sys.stderr.write('IOError on file ' + wav_filename + '\n' + \
            str(ioe) + '. Skipping.\n')
            return
        except EOFError as eofe:
            sys.stderr.write('EOFError on file ' + wav_filename + '\n' + \
            str(eofe) + '. Skipping.\n')
            return

        # Instantiate PyAudio.
        p = pyaudio.PyAudio()

        # Open stream.
        stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
            channels=wf.getnchannels(),
            rate=wf.getframerate(),
                        output=True)

        data = wf.readframes(chunk_size)
        while len(data) > 0:
            stream.write(data)
            data = wf.readframes(chunk_size)

        # Stop stream.
        stream.stop_stream()
        stream.close()

        # Close PyAudio.
        p.terminate()

#play_wav("output1.wav",CHUNK_SIZE)


