import re
import requests
from constants import BASE_URL, BASE_HEADER, FILE_HI, CHUNK_SIZE
from play import play_wav
from voice_upload_file import post_voice

# Patterns
__ENTRANCE_PATTERN = r'^(hello)'  # -
__POSTPONE_PATTERN = r'(postpone?..?.?) (\w*) (\w*)'  # next
__NEXT_MESSAGE_PATTERN = r'(next message|okay)'  # next
__REPLY_PATTERN = r'(reply)'  # next
__REPEAT_PATTERN = r'(repeat)'  # repeat
__DONE_PATTERN = r'(done)'  # done

# Constants
__HOURS_IN_ONE_DAY = 24

numwords = {}

def text2int(textnum):
    if not numwords:
        units = [
            "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
            "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
            "sixteen", "seventeen", "eighteen", "nineteen",
        ]

        tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

        scales = ["hundred", "thousand", "million", "billion", "trillion"]

        numwords["and"] = (1, 0)
        for idx, word in enumerate(units):  numwords[word] = (1, idx)
        for idx, word in enumerate(tens):       numwords[word] = (1, idx * 10)
        for idx, word in enumerate(scales): numwords[word] = (10 ** (idx * 3 or 2), 0)

    ordinal_words = {'first': 1, 'second': 2, 'third': 3, 'fifth': 5, 'eighth': 8, 'ninth': 9, 'twelfth': 12}
    ordinal_endings = [('ieth', 'y'), ('th', '')]

    textnum = textnum.replace('-', ' ')

    current = result = 0
    for word in textnum.split():
        if word in ordinal_words:
            scale, increment = (1, ordinal_words[word])
        else:
            for ending, replacement in ordinal_endings:
                if word.endswith(ending):
                    word = "%s%s" % (word[:-len(ending)], replacement)

            if word not in numwords:
                raise Exception("Illegal word: " + word)

            scale, increment = numwords[word]

        current = current * scale + increment
        if scale > 100:
            result += current
            current = 0


    return result + current
#testing
#x=text2int('two thousand fifty')
#print(x)
#print(type(x))


def __create_time_format(hours, minutes, seconds):
    hours_str = '0' if hours / 10 < 1 else ''
    minutes_str = '0' if minutes / 10 < 1 else ''
    seconds_str = '0' if seconds / 10 < 1 else ''

    try:
        hours_str += str(hours)  # ex. 01
        minutes_str += str(minutes)  # ex. 30
        seconds_str += str(seconds)  # ex. 30
    except ValueError:
        print("Could not convert amount of time to an integer. Loc: validate_voice > validate_voice")

    print('__create_time_format')
    print(hours_str + ':' + minutes_str + ':' + seconds_str)
    return hours_str + ':' + minutes_str + ':' + seconds_str



def postpone_request(message_id, postponed_time):
    if type(message_id) is not str:
        message_id = str(message_id)

    endpoint = 'voice-messages/' + message_id + '/postpone/' + postponed_time
    url = BASE_URL + endpoint

    resp = requests.post(url, headers=BASE_HEADER)
    return resp


def validate_voice(lex, message_id, from_account_id, to_account_id):
    """
    Takes action according to the lexical response as it is the command.
    :param lex: type of str. 'lex' implies this is expected as lexical response.
    :param message_id: type of int. id of the message
    :param from_account_id: type of int. id of the message sender
    :param to_account_id: type of int. id of the message receiver
    :return: None
    """
    #lex = 'postpone ten minutes'
    if re.search(__POSTPONE_PATTERN, lex, re.I) is not None:
        match_objects = re.search(__POSTPONE_PATTERN, lex, re.I)
        print('Im in postpone')
        amount = 0
        try:
            amount = text2int(match_objects.group(2))  # ex. 30
            print('the amount is ')
            print(amount)
        except ValueError:
            print('Could not convert amount of time to an integer. Loc: validate_voice > validate_voice')
        time_measure = match_objects.group(3)  # ex minutes

        if time_measure == 'minute' or time_measure == 'minutes':
            postponed_time = __create_time_format(hours=0, minutes=amount, seconds=0)
        elif time_measure == 'hour' or time_measure == 'hours':
            postponed_time = __create_time_format(hours=amount, minutes=0, seconds=0)
        elif time_measure == 'day' or time_measure == 'days':
            hours = amount * __HOURS_IN_ONE_DAY
            postponed_time = __create_time_format(hours=hours, minutes=0, seconds=0)
        else:
            postponed_time = ''
        print('sending request')
        postpone_request(message_id=message_id, postponed_time=postponed_time)

        return 'postpone'
    elif re.search(__NEXT_MESSAGE_PATTERN, lex, re.I) is not None:
        return 'next'
    elif re.search(__REPLY_PATTERN, lex, re.I) is not None:
        to_account_ids = [from_account_id]
        post_voice(from_account_id=to_account_id, to_account_ids=to_account_ids.__str__())

        return 'next'
    elif re.search(__REPEAT_PATTERN, lex, re.I) is not None:
        return 'repeat'
    elif re.search(__DONE_PATTERN, lex, re.I) is not None:
        return 'done'
    else:
        return 'not_valid'

#lex = 'can you postpone 30 minutes the message voiceit?'
#validate_voice(lex, message_id=100, from_account_id=3, to_account_id=4)