import pyaudio
import wave
import platform
from subprocess import call

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000


def record_wav(wav_filename, chunk_size,RECORD_SECONDS):

    if platform.system()=='Linux':

        path = "/home/pi/pywork/"
        call([ "arecord" ,"-vv" ,"-f", "S16_LE", "-D","plughw:1,0", "-r","16000","-d",RECORD_SECONDS,path+ wav_filename])

    else:

        p = pyaudio.PyAudio()

        stream = p.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        input=True,
                        frames_per_buffer=CHUNK)

        print("* recording")

        frames = []

        for i in range(0, int(RATE / CHUNK * int(RECORD_SECONDS))):
            data = stream.read(CHUNK)
            frames.append(data)

        print("* done recording")

        stream.stop_stream()
        stream.close()
        p.terminate()

        wf = wave.open(wav_filename, 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(p.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(b''.join(frames))
        wf.close()

#record_wav("testy3.wav", 1024,4)