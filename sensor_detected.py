import RPi.GPIO as GPIO
import time
from play import play_wav
import requests
from constants import BASE_URL
import os

def sensor_on():

    device_id=7 #asya_oda
    url_has_message=BASE_URL + "devices/" + str(device_id) + "/has-message"
    token = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
    headers = {'Authorization': token}

    has_message = requests.get(url_has_message, headers=headers)
    print(has_message.text)

    GPIO.setmode(GPIO.BCM)
    GPIO_PIR = 7
    
    CHUNK_SIZE = 1024
    
    GPIO.setup(GPIO_PIR, GPIO.IN)
    
    try:
    
        print ("PIR module test")
    
        time.sleep(2)
    
        print ("ready")
    
        #while True:
    
        if GPIO.input(GPIO_PIR):
            print("Motion detected")

            if has_message.text=="true" :
                print("device has message so said beep beep")

                play_wav("beep_beep.wav",CHUNK_SIZE)
                play_wav("beep_beep.wav", CHUNK_SIZE)

                time.sleep(3)

                is_somebody=True

            else:
                time.sleep(1)
                is_somebody=False
        else:
            is_somebody=False

        return is_somebody



    except KeyboardInterrupt:
        print ("Quit")
        GPIO.cleanup()

