import os

# Constant Numbers
CHUNK_SIZE = 1024

BASE_URL = 'http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/'
SKIP_TAGS = os.getenv('SKIP_TAGS', '').split()

# Base Header Fields
TOKEN_HEADER_KEY = 'Authorization'
TOKEN_HEADER_VALUE = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
CONTENT_TYPE_HEADER_KEY = 'Content-Type'
CONTENT_TYPE_HEADER_VALUE = 'application/json'
BASE_HEADER = {TOKEN_HEADER_KEY: TOKEN_HEADER_VALUE}

# Constant filenames
FILE_HI = 'hi.wav'


