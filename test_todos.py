# Third-party imports...
from nose.tools import assert_true
import requests



def test_request_response():
    url = 'http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/users/'

    # Send a request to the real API server and store the response.
    response = requests.get(url)


    # Confirm that the request-response cycle completed successfully.
    assert_true(response.ok)





