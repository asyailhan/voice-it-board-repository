from base_clients import get_base
from datetime import datetime,timedelta
from play import play_wav
from connect_S3_download import download_file
import requests
from validate_voice import validate_voice
from record import record_wav
from time import gmtime, strftime
from constants import BASE_URL, BASE_HEADER, CHUNK_SIZE
import json

def get_voice(id):
    token = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
    headers = {'Authorization': token, "Content-Type": "application/json"}
    USERS_URL = 'http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/voice-messages/pendings/'

    url_pendings=USERS_URL + str(id) + "?uuid=c12b3b99-f842-492a-ac3d-899465b0369d" #asya_oda

    response_list=get_base(url_pendings,headers)
    return response_list

def repeat_time_calculation(repeat_time):
    list = repeat_time.split(' ')
    time_added=0
    amount = int(list[0])
    print(amount)
    time_measure = list[1]
    print(time_measure)

    if(time_measure=="day"):
        time_added=amount*24*60
    elif(time_measure=="hour"):
        time_added=amount*60
    elif(time_measure=="minute"):
        time_added=amount
    else:
        print("unvalid convertion for repeat time!!")

    print(time_added)
    return time_added


def check_repeat_time(will_repeat_by_time,repeat_time,optional_id,expiration_date):
    token = "Token 8d0d44a7e75695f47709229a7901bc5808941b60"
    header = {"content-type": "application/json", "Authorization": token}

    if will_repeat_by_time==True:

        repeat_t=repeat_time_calculation(repeat_time)
        #s_date = datetime.strptime(set_date, "%Y-%m-%dT%H:%M:%S")

        string_time = strftime("%Y-%m-%dT%H:%M:%S", gmtime())  # eg: '2009-01-05T22:14:39'
        current_time = datetime.strptime(string_time, "%Y-%m-%dT%H:%M:%S")

        new_date=current_time + timedelta(minutes=repeat_t)

        str_time=new_date.strftime("%Y-%m-%dT%H:%M:%S")
        print(str_time)

        body = {
            'repeat_time': repeat_time,
            'expiration_date': expiration_date,
            'set_date': str_time,
            'will_repeat_by_time': True,
            'will_repeat': True,
            'is_set_time': True,
        }

        base_url="http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/voice-messages-optionals/"\
                 + str(optional_id) + '/'

        response = requests.put(base_url, data=json.dumps(body), headers=header)
        print("the repeat is done " + str(response.status_code))
        print(response.content)


def repeat_time(message_id):
    token = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
    headers = {'Authorization': token, "Content-Type": "application/json"}
    users_url = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/voice-messages/" + str(message_id) + "/"

    response = requests.get(users_url, headers=headers)
    list_resp = response.json()
    voice_message_opt = list_resp['voice_message_optionals']
    will_repeat_by_time = voice_message_opt['will_repeat_by_time']


    return will_repeat_by_time


def is_time_to_play(message_id):
    token = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
    headers = {'Authorization': token, "Content-Type": "application/json"}
    users_url="http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/voice-messages/" + str(message_id) + "/"

    response=requests.get(users_url,headers=headers)
    list_resp=response.json()
    voice_message_opt=list_resp['voice_message_optionals']
    optional_id = voice_message_opt['id']
    set_date=voice_message_opt['set_date']
    will_repeat_by_time=voice_message_opt['will_repeat_by_time']
    repeat_time=voice_message_opt['repeat_time']
    expiration_date = voice_message_opt['expiration_date']

    #testing
    """set_date="2016-12-21T01:33:00.458139"
    will_repeat_by_time=True
    repeat_time="1 minute"""

    if set_date!=None:
        compare_time=datetime.strptime(set_date, "%Y-%m-%dT%H:%M:%S")
        string_time=strftime("%Y-%m-%dT%H:%M:%S", gmtime()) #eg: '2009-01-05 22:14:39'
        current_time=datetime.strptime(string_time, "%Y-%m-%dT%H:%M:%S")

        print(compare_time)
        print(current_time)

        if compare_time < current_time: #if plays then repeats
            check_repeat_time(will_repeat_by_time, repeat_time, optional_id, expiration_date)
            return True
        else:
            return False
    else:
        return True

def validate_response(reply_filename,message_id,from_account_id, to_account_id, bucket_name, file_name):
    lexical = request_to_speech_recog(reply_filename)
    print(str(lexical))
    print(message_id)
    string_message = validate_voice(lexical, message_id, from_account_id, to_account_id)
    print(string_message)
    text = command_interpret(string_message, bucket_name, file_name, message_id, from_account_id, to_account_id)
    return text

def download_and_validate(bucket_name,file_name,message_id,from_account_id,to_account_id,repeat=False):
    if repeat or is_time_to_play(message_id)==True:

        filename_to_play = download_file(bucket_name, file_name)
        print("Playing soon.....")
        print(filename_to_play)
        play_wav(filename_to_play, chunk_size=CHUNK_SIZE)

        response_from_user()
        reply_filename = "reply.wav"

        return validate_response(reply_filename,message_id,from_account_id, to_account_id, bucket_name, file_name)


def helper_method(text, message_id, from_account_id, to_account_id, bucket_name, file_name):
    if text == "done_completely":
        return 'done'

    elif text == 'not_valid':
        play_wav("not_valid_response.wav", CHUNK_SIZE)
        reply_filename2 = "reply.wav"
        record_wav(reply_filename2, CHUNK_SIZE, "5")  # 5 sec to talk
        new_text=validate_response(reply_filename2, message_id, from_account_id, to_account_id, bucket_name, file_name)
        helper_method(new_text, message_id, from_account_id, to_account_id, bucket_name, file_name)
        return 'helper'
    else:
        return ''


def play_from_the_list(response):

    if response!=None:
        for voice_file in response:

            message_id=voice_file['id']
            from_account_id = voice_file['from_account_id']
            to_account_id = voice_file['to_account_id']
            bucket_url = voice_file['bucket_url']


            print(bucket_url)
            token = bucket_url.split("/")
            bucket_name = token[3]
            file_name = token[5]
            text=download_and_validate(bucket_name, file_name, message_id, from_account_id, to_account_id)
            new_text=helper_method(text, message_id, from_account_id, to_account_id, bucket_name, file_name)

            if new_text=='done':
             return

def command_interpret(user_message,bucket_name, file_name,message_id,from_account_id,to_account_id):
    token = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
    headers = {'Authorization': token, "Content-Type": "application/json"}
    read_URL = 'http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/voice-messages/' + message_id.__str__() + '/set-read'

    will_repeat_time_resp=repeat_time(message_id)

    if will_repeat_time_resp==False:
        if (not user_message == "postpone"):
            resp=requests.post(read_URL,headers=headers)
            print(str(resp.status_code) + "  is_read" )

    if user_message=='repeat':
        download_and_validate(bucket_name, file_name, message_id, from_account_id, to_account_id, True)
        return 'repeat'

    if user_message=='done':
        return "done_completely"
    if user_message=='not_valid':
        return "not_valid"



def response_from_user():
    reply_filename="reply.wav"

    print ("Please reply the message after the voice beep.")
    play_wav("please_reply.wav", CHUNK_SIZE) #please reply the message in 5 sec, after beep voice
    play_wav("beep_beep.wav",CHUNK_SIZE) #beeeep
    record_wav(reply_filename,CHUNK_SIZE,"5") #5 sec to talk

def request_to_speech_recog(filename):
    token = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
    headers = {'Authorization': token}
    url_speech_recog = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/speech-to-text"

    files = {"file": open(filename, 'rb')}

    response_speech_recog = requests.post(url_speech_recog, files=files, headers=headers)

    if response_speech_recog.status_code==204:
        print("empty message")
        play_wav("empty_message.wav",CHUNK_SIZE)

    elif response_speech_recog.status_code==200:
        lexical=response_speech_recog.json()
        print(lexical)
        return lexical


#resp_list=get_voice(58)
#play_from_the_list(resp_list)













