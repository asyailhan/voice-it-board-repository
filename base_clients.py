import requests
import json


USERS_URL = 'http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/users/'
token = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
headers={'Authorization': token, "Content-Type": "application/json"}
header_post={"Content-Type": "application/json"}
header_put={'Authorization': 'Bearer ' + token, "Content-Type": "application/json" }

def get_base(url_user,header_user):
    response = requests.get(url_user, headers=header_user)

    if response.ok:
        return response.json()

def post_base(url_user,header_user,body):
    response=requests.post(url_user,data=json.dumps(body),headers=header_user)
    return response.json()

def put_base(url_user,header_user,body):
    response = requests.put(url_user,data=json.dumps(body),headers=header_user)
    return response.json()

def delete_base(url_user,header_user,body):
    response = requests.delete(url_user,data=json.dumps(body), headers=header_user)
    return response.json()

#testcases
#content=get_base(USERS_URL,headers)
#print(content)


 #testcases
"""data={
    "username": "hpokjlkjl",
    "first_name": "yi",
    "last_name": "haancan",
    "email": "heakjsy@ahey.com",
    "password": "123heksjy"
}"""
#content_post=post_users(USERS_URL,header_post,data)
#con_delete=delete_users(USERS_URL,headers,data)



#testcases
"""
id='8'
url_ex=urljoin(USERS_URL, id)
url1=url_ex + "/"
print(url1)
url2="http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/users/8/"
data={
    "username": "asya",
    "first_name": "yi",
    "last_name": "haancan",
    "email": "heakjsy@ahey.com",
    "password": "123heksjy"
}
token1="Token 8d0d44a7e75695f47709229a7901bc5808941b60"
header1={"content-type": "application/json", "Authorization": token1 }
con=put_users(url1,header1,data)
print(con)"""
