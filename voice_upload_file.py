from constants import BASE_URL, BASE_HEADER
from record import record_wav
import requests
import platform

CHUNK_SIZE = 1024

VOICE_MES_URL="http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/upload/voice-messages"
token = 'Token 8d0d44a7e75695f47709229a7901bc5808941b60'
headers={'Authorization': token }



def post_voice(from_account_id, to_account_ids):

    device_id=7 #asya_oda

    try:
        endpoint = '/upload/voice-messages'
        url = BASE_URL + endpoint

        filename = 'voice_record.wav'
        record_wav(filename, CHUNK_SIZE, "5")

        body = {
            "from_account_id": from_account_id,
            "to_account_ids": to_account_ids,
            "from_device_id": device_id
        }

        files = {"file": open(filename, 'rb')}

        response = requests.put(url, files=files, data=body, headers=BASE_HEADER)

        print(response.text)
        print("done")
        return response
    except Exception as ex:
        print('Something went wrong. Error: ' + ex.__str__())
        return None



# Better to write a test for it, but you can uncomment it to run. Just DO NOT FORGET to comment it back. !* Careful *!
# post_voice('recorded_voice.wav')

